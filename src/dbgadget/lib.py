import dataset
import os
import sys
import logging

# get systemd logging is optional
try:
    import systemd.journal
except ImportError:
    pass

class dbgadget(object):
    engine_kwargs = {'connect_args': {'sslmode': 'require'}}
    def __init__(self):
        self.setup_logging()
    def setup_logging(self):
        self.lg = logging.getLogger(self.__class__.__name__)
        self.lg.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        logFormat = logging.Formatter(("%(asctime)s|%(name)s|%(levelname)s|%(message)s"))
        ch.setFormatter(logFormat)
        self.lg.addHandler(ch)
        if 'systemd' in sys.modules:
            sysL = systemd.journal.JournalHandler(SYSLOG_IDENTIFIER=self.lg.name)
            sysLogFormat = logging.Formatter(("%(levelname)s|%(message)s"))
            sysL.setFormatter(sysLogFormat)
            self.lg.addHandler(sysL)
    def connect(self):
        # note to self: echo DBGADGET_URI=secret > .env; source .env; export $(cut -d= -f1 .env|grep -vE "^#")
        db_uri = os.environ['DBGADGET_URI']
        self.db = dataset.connect(url=db_uri, engine_kwargs=self.engine_kwargs)
        psycopg_connection = self.db.engine.raw_connection().connection
        db_host = psycopg_connection.info.host
        if psycopg_connection.closed == 0:
            self.lg.debug(f"Connected to database hosted on {psycopg_connection.info.host}")
            if psycopg_connection.info.ssl_in_use == True:
                self.lg.debug(f"Secure SSL connection attributes follow:")
                for name in psycopg_connection.info.ssl_attribute_names:
                    self.lg.debug(f"{name} = {psycopg_connection.info.ssl_attribute(name)}")
            else:
                self.lg.warning(f"Unsecure DB connection discovered. Change your secrets now.")
                sys.exit(-1)
        else:
            self.lg.warning(f"Unable to establish database connection on {db_host}")
    def info(self):
        self.lg.info(f"The database contains the following tables:")
        for table_name in self.db.tables:
            self.lg.info(table_name)
    #def csv_dumper(self):
    #    if 


def test():
    ret_val = -1
    dbg = dbgadget()
    dbg.connect()
    dbg.info()

    ret_val = 0
    return ret_val

if __name__ == "__main__":
    exit_code = test()
    sys.exit(exit_code)